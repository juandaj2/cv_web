const nodemailer = require('nodemailer');
const jwt = require('jsonwebtoken')
const { google } = require("googleapis");
const OAuth2 = google.auth.OAuth2;

module.exports = (formulario, res) => {
    var email_target = formulario.email;
    var email_transmitter = formulario.name;
    var email_message = formulario.message;

    const createTransporter = async() => {
        const oauth2Client = new OAuth2(
            "823576042053-atnvav4ctld7naencvgt33s2pt9q05ar.apps.googleusercontent.com", //cliend_id
            "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", //client_secret
            "https://www.googleapis.com/oauth2/v4/token" //redirect_uri
        );

        oauth2Client.setCredentials({
            refresh_token: "1//05ufjNXbWdu1SCgYIARAAGAUSNwF-L9IrKlnGAL36YuQQCvOHu4w_pyy1zeIHXv1AbqXIco6_gInnawyqDDQMP5TmXAAQkTChXdM"
        });

        const accessToken = await new Promise((resolve, reject) => {
            oauth2Client.getAccessToken((err, token) => {
                if (err) {
                    reject("Failed to create access token :(");
                }
                resolve(token);
            });
        });

        const transporter = nodemailer.createTransport({
            service: "gmail",
            auth: {
                type: "OAuth2",
                user: "juan.daj17@gmail.com",
                accessToken,
                clientId: "823576042053-atnvav4ctld7naencvgt33s2pt9q05ar.apps.googleusercontent.com",
                clientSecret: "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",
                refreshToken: "1//05ufjNXbWdu1SCgYIARAAGAUSNwF-L9IrKlnGAL36YuQQCvOHu4w_pyy1zeIHXv1AbqXIco6_gInnawyqDDQMP5TmXAAQkTChXdM"
            }
        });

        return transporter;
    };

    const sendEmail = async(emailOptions) => {
        let emailTransporter = await createTransporter();
        await emailTransporter.sendMail(emailOptions, function(err, info) {
            if (err) {
                res.json({
                    ok: false,
                    message: 'El mensaje no pudo ser mandado',
                    err
                });
            } else {
                res.json({
                    ok: true,
                    message: 'El mensaje enviado',
                    info
                });
            }
        });
    };

    sendEmail({
        from: 'juan.daj17@gmail.com', // sender address
        to: "jdaragonj@uqvirtual.edu.co", // list of receivers
        cc: email_target,
        subject: 'CV_WEB_JUAN_ARAGON', // Subject line
        html: `<p>Correo: ${email_target} <br>Nombre: ${email_transmitter} <br>Mensaje: ${email_message}</p>` // plain text body
    });
}